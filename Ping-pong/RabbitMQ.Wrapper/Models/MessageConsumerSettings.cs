﻿using RabbitMQ.Client;
using System.Collections.Generic;

namespace RabbitMQ.Wrapper.Models
{
    public class MessageConsumerSettings
    {
        public bool SequentialFetch { get; set; } = true;

        public bool AutoAcknowlage { get; set; } = true;

        public IModel Chanel { get; set; }

        public string QueueName { get; set; }

        public string ConsumerTag { get; set; }

        public bool NoLocal { get; set; } = false;

        public bool Exclusive { get; set; } = false;

        public IDictionary<string, object> Arguments { get; set; }
    }
}
