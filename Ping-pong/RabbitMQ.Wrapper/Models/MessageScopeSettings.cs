﻿namespace RabbitMQ.Wrapper.Models
{
    public class MessageScopeSettings
    {
        public string ExchangeName { get; set; }

        public string QueueName { get; set; }

        public string RoutingKey { get; set; }

        /// <summary>
        /// Use fields from RabbitMQ.Client.ExchangeType
        /// </summary>
        public string ExchangeType { get; set; }
    }
}
