﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Text;

namespace RabbitMQ.Wrapper.Services.MessageProducer
{
    public class MessageProducer : IMessageProducer
    {
        private readonly MessageProducerSettings _messageProducerSettings;
        private readonly IBasicProperties _properties;

        public MessageProducer(MessageProducerSettings messageProducerSettings)
        {
            _messageProducerSettings = messageProducerSettings;

            _properties = _messageProducerSettings.Chanel.CreateBasicProperties();
            _properties.Persistent = true;
        }

        public void SendMessageToQueue(string message, string type = null)
        {
            if (!string.IsNullOrEmpty(type))
                _properties.Type = type;

            var body = Encoding.UTF8.GetBytes(message); 
            _messageProducerSettings.Chanel.BasicPublish(_messageProducerSettings.PublicationAddress, _properties, body);
        }

        public void SendMessageToQueueTyped(Type type, string message)
        { 
            SendMessageToQueue(message, type.AssemblyQualifiedName);
        }
    }
}
