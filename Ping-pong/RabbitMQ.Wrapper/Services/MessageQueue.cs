﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Services
{
    public class MessageQueue : IMessageQueue
    {
        private readonly IConnection _connection;

        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Chanel = _connection.CreateModel();
        }

        public MessageQueue(
            IConnectionFactory connectionFactory, 
            MessageScopeSettings messageScopeSettings) : this(connectionFactory)
        {
            DeclareExchange(messageScopeSettings.ExchangeName, messageScopeSettings.ExchangeType);

            if  (messageScopeSettings != null)
            {
                BindQueue(messageScopeSettings.ExchangeName, messageScopeSettings.RoutingKey, messageScopeSettings.QueueName);
            }
        }

        public IModel Chanel { get; protected set; }

        private void DeclareExchange(string exchangeName, string exchangeType)
        {
            Chanel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }

        private void BindQueue(string exchangeName, string routingKey, string queueName)
        {
            Chanel.QueueDeclare(queueName, true, false, false);
            Chanel.QueueBind(queueName, exchangeName, routingKey);
        }

        public void Dispose()
        {
            Chanel?.Dispose();
            _connection?.Dispose();
        }
    }
}
