﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Services.MessageConsumer
{
    public class MessageConsumerScopeFactory : IMessageConsumerScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IMessageConsumerScope Connect(MessageScopeSettings messageScopeSettings, MessageConsumerSettings messageConsumerSettings)
        {
            var mqConsumerScope = Open(messageScopeSettings, messageConsumerSettings);
            mqConsumerScope.MessageConsumer.Connect();

            return mqConsumerScope;
        }

        private IMessageConsumerScope Open(MessageScopeSettings messageScopeSettings, MessageConsumerSettings messageConsumerSettings)
        {
            return new MessageConsumerScope(_connectionFactory, messageScopeSettings, messageConsumerSettings);
        }
    }
}
