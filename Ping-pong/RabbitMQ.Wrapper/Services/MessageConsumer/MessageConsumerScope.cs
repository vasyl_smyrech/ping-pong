﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.Services.MessageConsumer
{
    public class MessageConsumerScope : IMessageConsumerScope
    {
        private readonly MessageScopeSettings _messageScopeSettings;
        private readonly MessageConsumerSettings _messageConsumerSettings;
        private readonly Lazy<IMessageQueue> _messageQueueLazy;
        private readonly Lazy<IMessageConsumer> _messageConsumerLazy;

        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScope(
            IConnectionFactory connectionFactory,
            MessageScopeSettings messageScopeSettings,
            MessageConsumerSettings messageConsumerSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;
            _messageConsumerSettings = messageConsumerSettings;

            _messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue);
            _messageConsumerLazy = new Lazy<IMessageConsumer>(CreateMessageConsumer);
        }

        public IMessageConsumer MessageConsumer => _messageConsumerLazy.Value;

        public IMessageQueue MessageQueue => _messageQueueLazy.Value;

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }

        private IMessageConsumer CreateMessageConsumer()
        {
            return new MessageConsumer(new MessageConsumerSettings
            {
                Chanel = MessageQueue.Chanel,
                QueueName = _messageScopeSettings.QueueName,
                ConsumerTag = _messageConsumerSettings.ConsumerTag,
                SequentialFetch = _messageConsumerSettings.SequentialFetch,
                Arguments = _messageConsumerSettings.Arguments,
                AutoAcknowlage = _messageConsumerSettings.AutoAcknowlage,
                Exclusive = _messageConsumerSettings.Exclusive,
                NoLocal = _messageConsumerSettings.NoLocal
            });
        }

        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSettings);
        }
    }
}
