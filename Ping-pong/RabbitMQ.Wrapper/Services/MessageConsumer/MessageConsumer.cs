﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.Services.MessageConsumer
{
    public class MessageConsumer : IMessageConsumer
    {
        private readonly MessageConsumerSettings _settings;
        private readonly EventingBasicConsumer _consumer;

        public event EventHandler<BasicDeliverEventArgs> Received
        {
            add => _consumer.Received += value;
            remove => _consumer.Received -= value;
        }

        public MessageConsumer(MessageConsumerSettings settings)
        {
            _settings = settings;

            _consumer = new EventingBasicConsumer(_settings.Chanel);
        }

        public void Connect()
        {
            if (_settings.SequentialFetch)
            {
                _settings.Chanel.BasicQos(0, 1, false);
            }

            _settings.Chanel.BasicConsume(
                _settings.QueueName,
                _settings.AutoAcknowlage,
                _settings.ConsumerTag,
                _settings.NoLocal,
                _settings.Exclusive,
                _settings.Arguments,
                _consumer);
        }

        public void SetAcknowlage(ulong deliveryTag, bool processed)
        {
            if (processed)
            {
                _settings.Chanel.BasicAck(deliveryTag, false);
            }
            else
            {
                _settings.Chanel.BasicNack(deliveryTag, false, true);
            }
        }
    }
}
