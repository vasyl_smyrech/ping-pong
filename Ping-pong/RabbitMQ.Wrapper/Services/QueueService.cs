﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Text;

namespace RabbitMQ.Wrapper.Services
{
    public class QueueService : IQueueService
    {
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScope _messageConsumerScope;

        public QueueService(
            IMessageProducerScopeFactory messageProducerScopeFactory,
            IMessageConsumerScopeFactory messageConsumerScopeFactory,
            MessageScopeSettings producerMessageScopeSettings,
            MessageScopeSettings consumerMessageScopeSettings,
            MessageConsumerSettings messageConsumerSettings
             )
        {
            _messageProducerScope = messageProducerScopeFactory.Open(producerMessageScopeSettings);

            _messageConsumerScope = messageConsumerScopeFactory.Connect(consumerMessageScopeSettings, messageConsumerSettings);
        }

        public Action<string> MessageReceived { get; set; }

        public bool SendMessageToQueue(string message)
        {
            try
            {
                _messageProducerScope.MessageProducer.SendMessageToQueue(message);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void ListenQueue()
        {
            _messageConsumerScope.MessageConsumer.Received += (object _, BasicDeliverEventArgs args) =>
            {
                var body = args.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                MessageReceived?.Invoke(message);
            };
        }
    }
}
