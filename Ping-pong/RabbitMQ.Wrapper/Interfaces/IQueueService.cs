﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IQueueService
    {
        Action<string> MessageReceived { get; set; }
        bool SendMessageToQueue(string message);
        void ListenQueue();
    }
}
