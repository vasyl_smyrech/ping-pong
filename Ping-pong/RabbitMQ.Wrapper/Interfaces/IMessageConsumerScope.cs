﻿namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScope
    {
        IMessageConsumer MessageConsumer { get; }
        IMessageQueue MessageQueue { get; }
    }
}
