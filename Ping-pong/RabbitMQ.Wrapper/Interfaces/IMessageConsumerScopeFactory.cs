﻿using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScopeFactory
    {
        IMessageConsumerScope Connect(MessageScopeSettings messageScopeSettings, MessageConsumerSettings messageConsumerSettings);
    }
}
