﻿namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducerScope
    {
        IMessageProducer MessageProducer { get; }
        IMessageQueue MessageQueue { get; }
    }
}
