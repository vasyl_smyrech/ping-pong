﻿using RabbitMQ.Wrapper.Interfaces;
using System;
using System.Configuration;
using System.Drawing;
using System.Threading;
using Console = Colorful.Console;

namespace Ponger
{
    internal class PongerConsole
    {
        private readonly IQueueService _queueService;
        private readonly string _pongerPostValue;

        internal PongerConsole(IQueueService queueService)
        {
            _queueService = queueService;

            _pongerPostValue = ConfigurationManager.AppSettings.Get("message");
        }

        internal void Start()
        {
            _queueService.ListenQueue();
            _queueService.MessageReceived += OnReceivedMessage;
        }

        private void OnReceivedMessage(string message)
        {
            Console.WriteLine($"Received {message} at {DateTime.Now}", Color.Aqua);

            var pingPongInterval = Int32.Parse(ConfigurationManager.AppSettings.Get("ping-pong-interval"));
            Thread.Sleep(pingPongInterval);

            SendMessage();
        }

        private void SendMessage()
        {
            _queueService.SendMessageToQueue(_pongerPostValue);
            Console.WriteLine($"Sent {_pongerPostValue} at {DateTime.Now}", Color.Chartreuse);
        }
    }
}