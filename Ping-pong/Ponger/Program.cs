﻿using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Services;
using RabbitMQ.Wrapper.Services.MessageConsumer;
using RabbitMQ.Wrapper.Services.MessageProducer;
using System;
using System.Configuration;
using Topshelf;

namespace Ponger
{
    public class Program
    {
        private static IServiceProvider _serviceProvider;

        static void Main(string[] _)
        {
            var rc = HostFactory.Run(x =>
            {
                x.Service<Program>(s =>
                {
                    s.ConstructUsing(name => new Program());
                    s.WhenStarted(tc => tc.StartProgram());
                    s.WhenStopped(tc => tc.DisposeServices());
                });
                x.EnableServiceRecovery(r => r.RestartService(TimeSpan.FromSeconds(10)));
                x.StartAutomatically();
                x.RunAsLocalSystem();
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }

        private void StartProgram()
        {
            RegisterServices();

            var console = new PongerConsole(_serviceProvider.GetRequiredService<IQueueService>());
            console.Start();
        }

        private void RegisterServices()
        {
            var connectionUri = ConfigurationManager.AppSettings.Get("rabbitmq-uri");
            var collection = new ServiceCollection()
                .AddSingleton<IConnectionFactory>(p => new ConnectionFactory()
                {
                    Uri = new Uri(connectionUri),
                })

                .AddScoped<IMessageConsumerScope, MessageConsumerScope>()
                .AddSingleton<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>()

                .AddScoped<IMessageProducerScope, MessageProducerScope>()
                .AddSingleton<IMessageProducerScopeFactory, MessageProducerScopeFactory>()

                .AddScoped<IQueueService>(p => new QueueService(
                    p.GetRequiredService<IMessageProducerScopeFactory>(),
                    p.GetRequiredService<IMessageConsumerScopeFactory>(),
                    new MessageScopeSettings
                    {
                        ExchangeName = ConfigurationManager.AppSettings.Get("producer-scope-exchange-name"),
                        ExchangeType = ConfigurationManager.AppSettings.Get("producer-scope-exchange-type"),
                        QueueName = ConfigurationManager.AppSettings.Get("producer-scope-queue-name"),
                        RoutingKey = ConfigurationManager.AppSettings.Get("producer-scope-routing-key")
                    },
                    new MessageScopeSettings
                    {
                        ExchangeName = ConfigurationManager.AppSettings.Get("consumer-scope-exchange-name"),
                        ExchangeType = ConfigurationManager.AppSettings.Get("consumer-scope-exchange-type"),
                        QueueName = ConfigurationManager.AppSettings.Get("consumer-scope-queue-name"),
                        RoutingKey = ConfigurationManager.AppSettings.Get("consumer-scope-routing-key")
                    },
                    new MessageConsumerSettings
                    {
                        ConsumerTag = $"{ConfigurationManager.AppSettings.Get("message-consumer-tag-prefix")}{new Guid()}"
                    }
                    ));

            _serviceProvider = collection.BuildServiceProvider();
        }

        private void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable disposable)
            {
                disposable.Dispose();
            }
        }
    }
}
