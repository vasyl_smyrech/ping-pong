﻿using RabbitMQ.Wrapper.Interfaces;
using System;
using System.Configuration;
using System.Drawing;
using System.Threading;
using Console = Colorful.Console;

namespace Pinger
{
    internal class PingerConsole
    {
        private readonly IQueueService _queueService;
        private readonly string _pingerPostValue;

        internal PingerConsole(IQueueService queueService)
        {
            _queueService = queueService;

            _pingerPostValue = ConfigurationManager.AppSettings.Get("message");
        }

        internal void Start()
        {
            _queueService.ListenQueue();
            _queueService.MessageReceived += OnMessageReceived;

            SendMessage();
        }

        private void OnMessageReceived(string message)
        {
            Console.WriteLine($"Received {message} at {DateTime.Now}", Color.BurlyWood);

            var pingPongInterval = Int32.Parse(ConfigurationManager.AppSettings.Get("ping-pong-interval"));
            Thread.Sleep(pingPongInterval);

            SendMessage();
        }

        private void SendMessage()
        {
            _queueService.SendMessageToQueue(_pingerPostValue);
            Console.WriteLine($"Sent {_pingerPostValue} at {DateTime.Now}", Color.CornflowerBlue);
        }
    }
}